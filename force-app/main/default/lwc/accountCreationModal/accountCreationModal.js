import { LightningElement, api, wire, track} from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import LightningModal from 'lightning/modal';    
export default class AccountCreationModal extends LightningModal {

    fields = ['Name', 'NumberOfEmployees', 'AccountNumber', 'OwnerId', 'Site', 'AccountSource', 'Active__c', 'AnnualRevenue', 'BillingAddress', 'CleanStatus', 'CreatedById', 'CustomerPriority__c', 'DandbCompanyId', 'DunsNumber', 'Jigsaw', 'Description', 'Tier', 'NumberOfEmployees', 'Fax', 'Industry', 'LastModifiedById', 'NaicsCode', 'NaicsDesc', 'NumberofLocations__c', 'OperatingHoursId', 'Ownership', 'ParentId', 'Phone', 'Rating', 'ShippingAddress', 'Sic', 'SicDesc', 'SLA__c', 'SLAExpirationDate__c', 'SLASerialNumber__c', 'TickerSymbol', 'Tradestyle', 'Type', 'UpsellOpportunity__c', 'Website', 'YearStarted'];
    objectName = 'Account';

    handleOkay() {

        this.close('okay');
    }

    handleSuccess(){

        console.log("inserted =>", this.recordId);
        this.close('okay');

    }
}