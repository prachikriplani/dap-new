import { LightningElement } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';

export default class Lwc_QuickAction extends LightningElement {

    closeAction(){
        this.dispatchEvent(new CloseActionScreenEvent());
    }
}